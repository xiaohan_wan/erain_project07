% Define the mappings from English words to their numeric equivalents.
word_number("zero", 0).
word_number("one", 1).
word_number("two", 2).
word_number("three", 3).
word_number("four", 4).
word_number("five", 5).
word_number("six", 6).
word_number("seven", 7).
word_number("eight", 8).
word_number("nine", 9).
word_number("ten", 10).
word_number("eleven", 11).
word_number("twelve", 12).
word_number("thirteen", 13).
word_number("fourteen", 14).
word_number("fifteen", 15).
word_number("sixteen", 16).
word_number("seventeen", 17).
word_number("eighteen", 18).
word_number("nineteen", 19).
word_number("twenty", 20).
word_number("thirty", 30).
word_number("forty", 40).
word_number("fifty", 50).
word_number("sixty", 60).
word_number("seventy", 70).
word_number("eighty", 80).
word_number("ninety", 90).
word_number("hundred", 100).
word_number("thousand", 1000).

 

% Convert a list of English words to a number.
to_num(Words, Num) :-
  phrase(number(Num), Words).

 

% The DCG rules for the number.
number(N) --> thousands(N).
number(N) --> hundreds(N).
number(N) --> tens(N).
number(N) --> ones(N).

 

ones(N) --> word(N).

 

tens(N) --> word(N1), ones(N2), {N is N1 + N2}.
tens(N) --> word(N).

 

hundreds(N) --> word(N1), ["hundred"], tens(N2), {N is N1 * 100 + N2}.
hundreds(N) --> word(N1), ["hundred"], ones(N2), {N is N1 * 100 + N2}.
hundreds(N) --> word(N1), ["hundred"], {N is N1 * 100}.
hundreds(N) --> tens(N).
hundreds(N) --> ones(N).

 

thousands(N) --> word(N1), ["thousand"], hundreds(N2), {N is N1 * 1000 + N2}.
thousands(N) --> word(N1), ["thousand"], tens(N2), {N is N1 * 1000 + N2}.
thousands(N) --> word(N1), ["thousand"], ones(N2), {N is N1 * 1000 + N2}.
thousands(N) --> word(N1), ["thousand"], {N is N1 * 1000}.
thousands(N) --> hundreds(N).
thousands(N) --> tens(N).
thousands(N) --> ones(N).

 

% Helper rule to lookup the numeric value of a word.
word(N) --> [W], {word_number(W, N)}.